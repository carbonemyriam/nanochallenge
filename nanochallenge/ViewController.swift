//
//  ViewController.swift
//  nanochallenge
//
//  Created by Myriam Carbone on 10/12/2019.
//  Copyright © 2019 Myriam Carbone. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    let movies = ["1","2","3","4","5","6","7","8","9","11","10"]
    let movieImage: [UIImage] = [
        
        UIImage(named:"1")!,
        UIImage(named:"2")!,
        UIImage(named:"3")!,
        UIImage(named:"4")!,
        UIImage(named:"5")!,
        UIImage(named:"6")!,
        UIImage(named:"7")!,
        UIImage(named:"8")!,
        UIImage(named:"9")!,
        UIImage(named:"10")!,
        UIImage(named:"11")!
       

      
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        // Do any additional setup after loading the view.
    }

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection: Int) -> Int {
        return movies.count
    }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
            
            cell.myImageView.image = movieImage[indexPath.item]
            return cell
    
    
}
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            return CGSize(width: self.collectionView.bounds.width, height: self.collectionView.bounds.height)
        }
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
        }
    
  
  
    
    
}
       
